package com.ana.blogapp.repository;

import org.springframework.data.repository.CrudRepository;

import com.ana.blogapp.entity.Post;

public interface PostRepository extends CrudRepository<Post, Integer> {

}
