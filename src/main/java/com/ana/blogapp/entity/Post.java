package com.ana.blogapp.entity;


import java.time.LocalDateTime;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;



@Table(name = "post")
@Entity
public class Post {
	
	

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
	
	@Column(name = "title", nullable = false, length = 50)
	private String title;
	
	@Column(name = "text", nullable = false, length = 1000)
	private String text;
	
	@Column(name = "creation_time", nullable = false)
	@CreationTimestamp
    private LocalDateTime creationTime;
	
	private Post() {}

    
    private Post(int id, String title, String text, LocalDateTime creationTime ) {
    	this.id = id;
        this.title = title;
        this.text = text;
        this.creationTime= creationTime;
    }
    public String getTitle() {
    	return title;
    }
    public String getText() {
    	return text;
    }
    public LocalDateTime getcreationTime() {
    	return creationTime;
    }
    public int getId() {
    	return id;
    }
     
    

	
}
