package com.ana.blogapp.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ana.blogapp.entity.Post;
import com.ana.blogapp.repository.PostRepository;


import lombok.AllArgsConstructor;

@Service
@AllArgsConstructor
public class PostService {
	@Autowired
    PostRepository postRepository;
	
	
	public Iterable<Post> getAllPosts() {
		return postRepository.findAll();
	}
	
	public Optional<Post> getPostById(int id) {
		return postRepository.findById(id);
	}
	public void deletePost(int id) {
		postRepository.deleteById(id);
	}
	public Post CreatePost(Post post) {
		return postRepository.save(post);
	}
	
}
