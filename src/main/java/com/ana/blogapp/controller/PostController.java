package com.ana.blogapp.controller;

import com.ana.blogapp.entity.Post;
import com.ana.blogapp.repository.PostRepository;
import com.ana.blogapp.service.PostService;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;





@RestController
@CrossOrigin(origins = {"http://localhost:8080","http://localhost:3000"})
@RequestMapping("posts")
public class PostController {

    @Autowired
    PostService postService; 

    @RequestMapping(method = RequestMethod.GET)
    public Iterable<Post> getPosts()  {
    	return postService.getAllPosts();
    	
    }

    

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void deletePostById(@PathVariable("id") int id) {
    	  postService.deletePost(id);
    }
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public Post getPostById(@PathVariable("id") int id) {
    	Optional<Post> iPost= postService.getPostById(id);
    	if(iPost.isPresent()) {
    		return iPost.get();
    	} else {
    		throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Post with id: "+ id+" not found.");
    	}
    
    }
   
    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
    public Post createPost(@RequestBody Post post) {
    	return postService.CreatePost(post);		
    }
}
