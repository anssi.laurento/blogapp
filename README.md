# Blog app

<!-- TOC depthFrom:1 depthTo:2 withLinks:1 updateOnSave:1 orderedList:0 -->

- [About](#about)
- [Setup and running the app](#Installation-and-running-the-app)

<!-- /TOC -->

## About

Blogapp is jar-packaged blog app with Java Backend and React.js Frontend.  
With this app you can read, add and delete posts with your web browser.  
Blog posts are stored in h2 database.

## Installation and running the app

### Prerequisites

Maven is needed to compile to app.  
Java is needed to compile and run the app.

Download compatible version of Maven from:  
https://maven.apache.org/

Download compatible version of Java from:  
https://www.java.com/ES/download/

### Installation

`> mvn clean install`

### Running the application

In target folder:

`> java -jar blogapp.jar`
