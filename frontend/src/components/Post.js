import React from "react";
import { format, parseISO } from "date-fns";
import { Link, useParams } from "react-router-dom";
import usePostService from "../services/usePostService";

const Post = () => {
  let { id } = useParams();

  const postService = usePostService(id);

  return (
    <div className="row pt-5">
      <div className="col-12 col-lg-6 offset-lg-3">
        <Link to="/">
          <button className="btn btn-success float-left">Back</button>
        </Link>

        {postService.result.status === "loading" && <div>Loading...</div>}
        {postService.result.status === "error" && (
          <div>Post with this id can't be found</div>
        )}
      </div>
      {postService.result.status === "loaded" && (
        <div className="col-12 col-lg-6 offset-lg-3">
          <div className="card my-2" key={postService.result.payload.id}>
            <div className="card-header">
              <div>{postService.result.payload.title}</div>
              <time>
                {format(
                  parseISO(postService.result.payload.creationTime.toString()),
                  "Pp"
                )}
              </time>
            </div>
            <div className="card-body">
              <div>{postService.result.payload.text}</div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
};

export default Post;
