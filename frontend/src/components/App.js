import React from "react";
import Posts from "./Posts";
import Post from "./Post";
import Form from "./Form";
import usePostsService from "../services/usePostsService";
import usePostPostService from "../services/usePostPostService";
import useDeletePostService from "../services/useDeletePostService";
import Header from "./Header";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

const App = () => {
  let postsService = usePostsService();
  let postpostService = usePostPostService();
  let deletepostService = useDeletePostService();

  const reFetch = () => {
    postsService.fetchPosts();
  };

  return (
    <div className="App">
      <Header />
      <Router>
        <Switch>
          <Route path="/new">
            <Form makePost={postpostService.makePost} reFetch={reFetch}></Form>
          </Route>
          <Route path="/post/:id">
            <Post />
          </Route>

          <Route path="/">
            <Posts
              postsService={postsService.result}
              reFetch={reFetch}
              deleteService={deletepostService}
            />
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default App;
