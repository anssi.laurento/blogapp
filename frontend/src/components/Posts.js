import React from "react";
import { MoonLoader } from "react-spinners";
import { format, parseISO, compareDesc } from "date-fns";
import { Link } from "react-router-dom";

const Posts = ({ deleteService, postsService, reFetch }) => {
  const deleteHandler = async e => {
    const id = e.target.getAttribute("name");
    await deleteService.deletePost(id);
    reFetch();
  };

  return (
    <div className="postsContainer">
      <div className="row pt-5">
        <div className="col-12 col-lg-6 offset-lg-3">
          <Link to="/new">
            <button className="btn btn-success float-left">Add new</button>
          </Link>
        </div>
        <div className="col-12 col-lg-6 offset-lg-3">
          <h1 className="text-center">Blog posts</h1>

          {postsService.status === "loading" && (
            <div className="col-12 col-lg-6 offset-lg-5">
              <MoonLoader />
            </div>
          )}

          {postsService.status === "loaded" &&
            postsService.payload
              .sort(function(a, b) {
                return compareDesc(
                  parseISO(a.creationTime.toString()),
                  parseISO(b.creationTime.toString())
                );
              })
              .map(post => (
                <div className="card my-2" key={post.id}>
                  <div className="card-header">
                    <div>{post.title}</div>
                    <time>
                      {format(parseISO(post.creationTime.toString()), "Pp")}
                    </time>
                  </div>
                  <div className="card-body">
                    <button
                      name={post.id}
                      onClick={deleteHandler}
                      className="btn btn-danger float-right"
                    >
                      Delete
                    </button>
                    <Link to={{ pathname: "/post/" + post.id, post: post }}>
                      <button className="btn btn-primary float-right">
                        View
                      </button>
                    </Link>
                  </div>
                </div>
              ))}
        </div>
      </div>

      {postsService.status === "error" && (
        <div className="col-12 col-lg-6 offset-lg-3">
          <div>Loading posts failed!</div>
          <button className="btn btn-primary" onClick={reFetch}>
            Try again
          </button>
        </div>
      )}
    </div>
  );
};

export default Posts;
