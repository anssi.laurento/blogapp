import React from "react";
import { useForm } from "react-hook-form";
import { Button, ButtonToolbar } from "react-bootstrap";
import { useHistory } from "react-router-dom";

const Form = props => {
  const { register, handleSubmit, errors } = useForm();
  let history = useHistory();

  const onSubmit = async data => {
    await props.makePost(data);
    await props.reFetch();
    history.push("/");
  };

  const onCancel = () => {
    history.push("/");
  };

  return (
    <div className="col-12 col-lg-6 offset-lg-3">
      <form onSubmit={handleSubmit(onSubmit)}>
        <input
          className="form-control my-3"
          name="title"
          placeholder="Title"
          ref={register({ required: true, minLength: 5, maxLength: 50 })}
        />
        {errors.title && (
          <p>Title lenght must be between 5 and 50 characters</p>
        )}

        <textarea
          className="form-control my-3"
          name="text"
          placeholder="Text"
          ref={register({ required: true, minLength: 40, maxLength: 1000 })}
        />
        {errors.text && (
          <p>Text lenght must be between 40 and 1000 characters</p>
        )}
        <ButtonToolbar>
          <Button variant="danger" onClick={onCancel}>
            Cancel
          </Button>
          <Button type="submit">Submit</Button>
        </ButtonToolbar>
      </form>
    </div>
  );
};

export default Form;
