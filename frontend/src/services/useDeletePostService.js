import { useState } from "react";
import { URL } from "../constants/constants";

const useDeletePostService = () => {
  const [service, setService] = useState({
    status: "init"
  });

  const deletePost = id => {
    setService({ status: "loading" });

    return new Promise((resolve, reject) => {
      fetch(URL + "posts/" + id, {
        method: "DELETE",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        referrer: "no-referrer"
      })
        .then(response => {
          setService({ status: "loaded" });
          resolve(response);
        })
        .catch(error => {
          setService({ status: "error", error });
          reject(error);
        });
    });
  };

  return {
    service,
    deletePost
  };
};

export default useDeletePostService;
