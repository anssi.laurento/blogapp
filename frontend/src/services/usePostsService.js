import { useState, useEffect } from "react";
import { URL } from "../constants/constants";

const usePostsService = () => {
  const [result, setResult] = useState({
    status: "loading"
  });

  const fetchPosts = () => {
    setResult({ status: "loading" });
    fetch(URL + "posts")
      .then(response => {
        if (response.status === 404) {
          return [];
        } else if (response.ok) {
          return response.json();
        } else {
          console.log(response.statusText);
          throw Error(response.statusText);
        }
      })
      .then(response => {
        setResult({ status: "loaded", payload: response });
      })
      .catch(error => {
        setResult({ status: "error", error });
      });
  };
  useEffect(() => {
    fetchPosts();
  }, []);
  return { result, fetchPosts };
};

export default usePostsService;
