import { useState, useEffect } from "react";
import { URL } from "../constants/constants";

const usePostService = id => {
  const [result, setResult] = useState({
    status: "loading"
  });

  const getPostById = id => {
    setResult({ status: "loading" });
    fetch(URL + "posts/" + id)
      .then(response => {
        if (response.status === 404) {
          throw Error(response.statusText);
        } else if (response.ok) {
          return response.json();
        } else {
          console.log(response.statusText);
          throw Error(response.statusText);
        }
      })
      .then(response => {
        setResult({ status: "loaded", payload: response });
      })
      .catch(error => {
        setResult({ status: "error", error });
      });
  };
  useEffect(() => {
    getPostById(id);
  }, [id]);
  return { result };
};

export default usePostService;
