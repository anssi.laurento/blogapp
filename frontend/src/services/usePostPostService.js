import { useState } from "react";
import { URL } from "../constants/constants";

const usePostPostService = () => {
  const [service, setService] = useState({
    status: "init"
  });

  const makePost = post => {
    setService({ status: "loading" });

    return new Promise((resolve, reject) => {
      fetch(URL + "posts", {
        method: "POST",
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin",
        headers: {
          "Content-Type": "application/json"
        },
        referrer: "no-referrer",
        body: JSON.stringify(post)
      })
        .then(response => response.json())
        .then(response => {
          setService({ status: "loaded", payload: response });
          resolve(response);
        })
        .catch(error => {
          setService({ status: "error", error });
          reject(error);
        });
    });
  };

  return {
    service,
    makePost
  };
};

export default usePostPostService;
